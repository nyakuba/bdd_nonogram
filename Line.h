/*
 * Line.h
 *
 *  Created on: Oct 11, 2013
 *      Author: nick_yakuba
 */

#ifndef LINE_H_
#define LINE_H_

#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::endl;

#include "buddy/bdd.h" //bdd library

/**
 *  Обертка под массив чисел, задающих начальные условия.
 */
class Line {
public:
	static int ithvar;
	static int max_ithvar;
	static bdd **vars;
	static unsigned h;
	static unsigned v;

	vector<int> nums;

	Line(bool v, int number) :
			isVertical(v), number(number) {
	}

	static void set_max_ithvar(int mith);
	static int inc_ithvar();
	bool isV();
	void setSize(unsigned hcounter, unsigned vcounter);
	int sum(int start = 0);
	void print();
	bdd req();
	void form_req(int *sp, int bankpos, int len, bdd &res);
	void add_req(int *spaces, bdd &res);

private:
	bool isVertical; //otherwise it is horizontal
	int number; //number of row/column.
};

#endif /* LINE_H_ */
