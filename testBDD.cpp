/**************************************************************************
 BDD nonogram solver.
 -----------------------------------------------
 Input file contains sequences of numbers,
 that describe blocks of nearby located cells, that should be filled.

 For example this text in file
 -1 2
 -1 2
 -1 2
 -2 1 1
 -2 3
 -2 1
 describes this grid:
 ___2 2 2
 11|_|_|_|
 _3|_|_|_|
 _1|_|_|_|

 Variable indexes:
 1 4 7
 2 5 8
 3 6 9

 One solution is then that 1,2,5,6,7,8 should be true, meaning:

 0 _ 0
 0 0 0
 _ 0 _

 **************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <fstream>

using std::vector;
using std::cout;
using std::endl;
using std::fstream;

#include "buddy/bdd.h" //bdd library

#include "Line.h"

int Line::ithvar;
int Line::max_ithvar;
bdd **Line::vars;
unsigned Line::h;
unsigned Line::v;

int main(int argc, char **argv) {
	//get options
	//TODO realize file input
	const char *filename;
	if (argc > 1)
		filename = argv[1];
	else
		return EXIT_SUCCESS;

	/** input file structure:
	 * v num1 num2 ... numN
	 * ...
	 * h num1 num2 ... numN
	 * nums must be positive
	 * -1 means vertical sequence of numbers
	 * -2 means horizontal sequence of numbers
	 * nums are divided by spaces.
	 * order: left to right, top to down */

	fstream in(filename);
	if (!in.is_open()) {
		perror("cannot open a file");
		return EXIT_FAILURE;
	}

	//Building requirements. And divide it into tasks.
	vector<Line> lines;

	unsigned vcounter = 0;
	unsigned hcounter = 0;
	int data;

	while (!in.eof()) {
		in >> data;

		switch (data) {
		case -1:
			lines.push_back(Line(true, vcounter++));
			break;
		case -2:
			lines.push_back(Line(false, hcounter++));
			break;
		default:
			lines.back().nums.push_back(data);
		}
	}

	//it works
//	for(unsigned i=0 ; i< lines.size(); i++){
//		lines[i].print();
//	}

	bdd_init(hcounter * vcounter * 256, 10000);
	int max_ithvar = hcounter * vcounter * 2;
	bdd_setvarnum(max_ithvar);

	bdd **vars = new bdd*[vcounter];
	for (unsigned i = 0; i < vcounter; i++) {
		vars[i] = new bdd[hcounter];
	}
	int k = 0;
	for (unsigned i = 0; i < vcounter; i++) {
		for (unsigned j = 0; j < hcounter; j++)
			vars[i][j] = bdd_ithvar(k++);
	}

//	bdd_printtable(vars[0][5]);
//	bdd_printtable(vars[5][0]); //err

	lines[0].max_ithvar = max_ithvar;
	lines[0].ithvar = k;
	lines[0].vars = vars;
	lines[0].setSize(hcounter, vcounter);

	bdd req;
//	cout << "line 0 of " << vcounter + hcounter << endl;
//	req = lines[0].req();
//	for (unsigned i = 1; i < lines.size(); i++) {
//		cout << "line " << i << " of " << vcounter + hcounter << endl;
//		req &= lines[i].req();
//	}

	unsigned divide = lines.size()/2;
	bdd *temp = new bdd[divide];
	for(unsigned i=0, k=0; i<divide; i++, k+=2){
		cout<<"add line "<< k << endl;
		temp[i] = lines[k].req();
		cout<<"add line "<< k+1 << endl;
		temp[i] &= lines[k+1].req();
	}
	if(divide%2 == 1){
		cout<<"add line "<< lines.size()-1 << endl;
		temp[0] &= lines[lines.size()-1].req();
	}

	while(divide >= 2){
		for(unsigned i=0, k=0; i<divide/2; i++, k+=2){
			cout<<"add + add "<<i<<endl;
			temp[i] &= temp[k];
			temp[i] &= temp[k+1];
		}
		if(divide%2 == 1){
			cout<<"+add "<<divide-1<<endl;
			temp[0] &= temp[divide-1];
		}
		divide/=2;
	}

	req = temp[0];
	req &= temp[1];

	delete [] temp;

//	bdd_printtable(req);
//	cout << "There are " << bdd_satcount(req) << " solutions\n";
//	cout << "one is:\n";
	bdd solution = bdd_satone(req);
	cout << "Solution:" << endl << bddset << solution << endl;

	bdd_printmatrix(solution.get_root(), vcounter, hcounter);

	cout << "exclude solution" << endl;
	req = req & !solution;

	cout << "There are " << bdd_satcount(req) << " solutions yet\n";
	cout << "one is:\n";
	solution = bdd_satone(req);
	cout << bddset << solution << endl;

	bdd_done();
}
