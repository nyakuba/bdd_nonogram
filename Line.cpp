/*
 * Line.cpp
 *
 *  Created on: Oct 11, 2013
 *      Author: nick_yakuba
 */

#include "Line.h"

void Line::set_max_ithvar(int mith) {
	max_ithvar = mith;
}

int Line::inc_ithvar() {
	if (ithvar == max_ithvar) {
		max_ithvar = ithvar + 1000;
		bdd_setvarnum(max_ithvar);
	}
	return ithvar++;
}

bool Line::isV() {
	return isVertical;
}

void Line::setSize(unsigned hcounter, unsigned vcounter) {
	h = hcounter;
	v = vcounter;
}

int Line::sum(int start /*= 0*/) {
	int sum = 0;
	for (unsigned i = start; i < nums.size(); i++) {
		sum += nums[i];
	}
	return sum;
}

void Line::print() {
	cout << "isVertical : " << isVertical << ", num :" << number << endl;
	for (unsigned i = 0; i < nums.size(); i++) {
		cout << nums[i] << " ";
	}
	cout << endl;
}

bdd Line::req() {
	unsigned total = sum() + nums.size() - 1;
	unsigned length = (isVertical) ? h : v;

	//debug
//		cout<< "total = "<<total << " > length = "<<length <<endl;

	if (total > length)
		return bdd();

	// число промежутков в последовательности
	int spacecount = nums.size() + 1;
	int *spaces = new int[spacecount];
	for (int i = 0; i < spacecount; i++)
		spaces[i] = 0;
	int bank = length - total;

	bdd result;

	//банк изначально в последней ячейке
	spaces[spacecount - 1] = bank;

	add_req(spaces, result); //calculate reqs, using sequence of spaces in result.
	for (int i = 0; i < bank; i++) {
		spaces[spacecount - 2]++;
		spaces[spacecount - 1]--;

		add_req(spaces, result);
		// param массив пропусков, ячейка с банком, размер массива, результат.
		//массивы должны быть независимыми. нужно будет их копировать.
		form_req(spaces, spacecount - 2, spacecount, result);
	}

	delete[] spaces;

	return result;
}

void Line::form_req(int *sp, int bankpos, int len, bdd &res) {
	//на каждом шаге массив пропусков должен иметь вид: {0,0,0..,0,n}
	//если в банке один элемент (n=1) - тупо считаем res
	if (sp[bankpos] == 1) {
		sp[bankpos] = 0;
		for (int i = 0; i < bankpos; i++) {
			sp[i] = 1;
			add_req(sp, res);
			sp[i] = 0;
		}
		//возвращаем исходное состояние массива. это состояние уже добавлено ранее
		sp[0] = 0;
		sp[bankpos] = 1;
	} else {
		// копируем массив
		int *spaces = new int[len];
		for (int i = 0; i < len; i++)
			spaces[i] = sp[i];

		int bank = sp[bankpos];
		for (int i = 0; (i < bank) && (bankpos - 1 >= 0); i++) {
			spaces[bankpos - 1]++;
			spaces[bankpos]--;

			add_req(spaces, res);
			//обрабатываем далее по рекурсии.
			if ((bankpos - 1) > 0)
				form_req(spaces, bankpos - 1, len, res);
		}
		delete[] spaces;
	}
	return;
}

void Line::add_req(int *spaces, bdd &res) {
	//первый элемент spaces - начало первого блока.
	//начало второго блока - размер первого + второй промежуток + 1.
	//третьего - начало второго + размер второго + третий промежуток + 1...

	bdd temp;
	bool isInitialized = false;

	//пустой столбец/строка
	if ((nums.size() == 1) && (nums[0] == 0)) {
		if (isVertical) {
			temp = temp = !vars[number][0];
			for (unsigned j = 1; j < h; j++)
				temp &= !vars[number][j];
		} else {
			temp = temp = !vars[0][number];
			for (unsigned j = 1; j < v; j++)
				temp &= !vars[j][number];
		}
		res |= temp;
		return;
	}

	int length = (isVertical) ? h : v;
	//полностью заполненный столбец/строка
	if ((nums.size() == 1) && (nums[0] == length)) {
		if (isVertical) {
			temp = temp = vars[number][0];
			for (unsigned j = 1; j < h; j++)
				temp &= vars[number][j];
		} else {
			temp = temp = vars[0][number];
			for (unsigned j = 1; j < v; j++)
				temp &= vars[j][number];
		}
		res |= temp;
		return;
	}

	//случай, когда начальный разрыв ненулевой
	if (spaces[0] != 0) {
		if (isVertical) {
			temp = !vars[number][0];
			isInitialized = true;
		} else {
			temp = !vars[0][number];
			isInitialized = true;
		}
		for (int j = 1; j < spaces[0]; j++) {
			if (isVertical) {
				temp &= !vars[number][j];
			} else {
				temp &= !vars[j][number];
			}
		}
	}

	int start = spaces[0];
	for (unsigned i = 0; i < nums.size(); i++) {
		//записываем те места, где размещаются 1.
		for (int j = start; j < (start + nums[i]); j++) {

			if (isVertical) {
				//column number, row j
				if (!isInitialized) {
					temp = vars[number][j];
					isInitialized = true;
				} else
					temp &= vars[number][j];
			} else {
				if (!isInitialized) {
					temp = vars[j][number];
					isInitialized = true;
				} else
					temp &= vars[j][number];
			}
		}

		//если столбец/строка не единственная, то обрабатываем пустые участки после.
		if ((isVertical && h > 1) || (!isVertical && v > 1)) {
			int prevstart = start;
			start += (nums[i] + spaces[i + 1] + 1);

			//возможен выход за границы массива.
			if (start > length)
				start = length;

			for (int j = prevstart + nums[i]; j < start; j++) {
				if (isVertical) {
					temp &= !vars[number][j];
				} else {
					temp &= !vars[j][number];
				}
			}
		}
	}

	res |= temp;

	//debug
//		for(unsigned i=0; i<nums.size()+1; i++) cout<<spaces[i]<<" ";
//		cout<<endl;
//		bdd_printtable(res);

	return;
}
